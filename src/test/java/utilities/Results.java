package utilities;

import java.util.HashMap;
import java.util.Map;

public class Results {
    private static Map<Integer, String> failureMessage = new HashMap<>();
    public static String passedMessage;

    static String getDescription() {
//        return new TestInitReference().getTestDescription();
        return null;
    }

    public static void setFailureMsg(String value) {
        failureMessage.put((int) (Thread.currentThread().getId()), value);
    }

    public static String getFailureMsg() {
        return failureMessage.get((int) (Thread.currentThread().getId()));
    }

    public static void pass(String value) {
        String msg = "[PASSED] "+ value;
        Log.info(msg);
        passedMessage = value;
    }

    public void pass() {
        pass(getDescription());
    }

    public static void fail(String value) {
        String msg = "[FAILED] " + value;
        Log.error(msg);
        setFailureMsg(msg);
    }

    public void fail(){
        fail(getDescription());
    }

    public static void skip(String value) {
        String msg = "[SKIPPED] "+ value;
        Log.warn(msg);
    }

    public static void warn(String value) {
        String msg = "[WARNING] " + value;
        Log.warn(msg);
    }

    public void warn() {
        warn(getDescription());
    }

    public void skip() {
        skip(getDescription());
    }

    public static void info(String value) {
        Log.info(value);
    }
}
