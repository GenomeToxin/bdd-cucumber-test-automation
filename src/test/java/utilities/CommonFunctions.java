package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.security.SecureRandom;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static utilities.Results.*;

public class CommonFunctions {

    private static final int timeout = 2;
    public static final WebDriverWait wait = new WebDriverWait(driver(), timeout);
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static WebDriver driver() {
        return DriverManager.getDriver();
    }

    public static void click(By by) {
        handleIframe(by);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        wait.until(ExpectedConditions.elementToBeClickable(by));
        WebElement element = driver().findElement(by);
        element.click();
    }

    public static void type(String str, By by) {
        WebElement element = driver().findElement(by);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        element.click();
        element.clear();
        element.sendKeys(str);
    }

    public static boolean isElementVisible(By by) {
        boolean value = false;
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
            if (driver().findElement(by).isDisplayed()) {
                value = true;
            }
            return value;
        } catch (Exception e) {
            return false;
        }
    }

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static void uploadFile(By by, String path) {
        driver().findElement(by).sendKeys(path);
    }

    public static void sleepForSeconds(int time) throws InterruptedException {
        TimeUnit.SECONDS.sleep(time);
    }

    public static boolean isElementPresent(By by) {
        try {
            WebElement element = driver().findElement(by);
            info("Element found in DOM: " + element.toString());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void waitForElementToBeVisible(By by) {
        for (int second = 0; ; second++) {
            if (second >= 15)
                break;
            if (isElementVisible(by))
                break;
        }
    }

    public static void waitForElementToBeInvisible(By by) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public static void handleIframe(By by) {
        driver().switchTo().defaultContent();
        int i = 0;
        while (!isElementVisible(by) && i <= 3) {
            info("Switching on iframe index: " + i);
            try {
                driver().switchTo().frame(i);
            } catch (NoSuchFrameException e) {
                warn("no such frame");
            }
            i++;
        }
        driver().switchTo().defaultContent();
    }

    public static String getAbsolutePath(String contentPath) {
        return new File(contentPath).getAbsolutePath();
    }

    public static void executeFile(String contentPath) throws Exception {
        Runtime.getRuntime().exec(getAbsolutePath(contentPath));
    }

    public static List<WebElement> findElements(By by) {
        return driver().findElements(by);
    }

    public static int getNumberOfElements(By by) {
        return findElements(by).size();
    }
}
