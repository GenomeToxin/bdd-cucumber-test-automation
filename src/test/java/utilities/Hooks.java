package utilities;

import io.cucumber.java.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static utilities.Results.info;

public class Hooks {
    FileReader reader = new FileReader("src/test/java/config.properties");
    Properties properties = new Properties();

    public Hooks() throws FileNotFoundException {
    }

    @Before(order=1)
    public void beforeScenario() throws IOException {
        properties.load(reader);
        DriverManager.setWebDriver((String) properties.get("browser"), properties.getProperty("url"));
        info("Start the browser and Clear the cookies");
    }
    @Before(order=0)
    public void beforeScenarioStart(){
        System.out.println("-----------------Start of Scenario-----------------");
    }

    @After(order=0)
    public void afterScenarioFinish(){
        System.out.println("-----------------End of Scenario-----------------");
    }
    @After(order=1)
    public void afterScenario(){
        DriverManager.removeDriver();
        info("Log out the user and close the browser");
    }
}
