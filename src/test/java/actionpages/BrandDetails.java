package actionpages;

import utilities.BasePage;

import static pageobjects.InfluencerDetailsPO.*;
import static utilities.CommonFunctions.*;

public class BrandDetails extends BasePage {

    public void uploadLogo(String path) {
        handleIframe(BTN_UPLOAD_LOGO);
        if (getNumberOfElements(LIST_CAMPAIGN) != 1) {
            click(BTN_UPLOAD_LOGO);
        }
        uploadFile(INPUT_UPLOAD_LOGO, path);
    }

    public void enterBrandName(String brandName) {
        type(brandName, TXT_BRAND_NAME);
    }

    public void clickSaveBrandLogo() {
        if (isElementPresent(ICO_ADD_BRAND_LOGO_DISABLED)) {
            waitForElementToBeInvisible(ICO_ADD_BRAND_LOGO_DISABLED);
            waitForElementToBeVisible(ICO_ADD_BRAND_LOGO);
        }
        click(ICO_ADD_BRAND_LOGO);
    }

    @Override
    public void waitPageToLoad() {
        super.waitPageToLoad();
    }
}
