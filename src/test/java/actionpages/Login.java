package actionpages;

import org.junit.Assert;
import utilities.BasePage;

import static pageobjects.HomepagePO.*;
import static pageobjects.LoginPO.*;
import static utilities.CommonFunctions.*;

public class Login extends BasePage {

    public void enterEmail(String str) {
        waitPageToLoad();
        type(str, TXT_EMAIL_LOGIN);
    }

    public void enterPassword(String password){
        type(password, TXT_PASSWORD_LOGIN);
    }

    public void clickLogin() {
        click(BTN_LOGIN);
    }

    public void verifyLoginSuccessful() {
        Assert.assertTrue(isElementVisible(HOMEPAGE_HEADER));
    }

    @Override
    public void waitPageToLoad() {
        super.waitPageToLoad(FORM_LOGIN);
    }
}
