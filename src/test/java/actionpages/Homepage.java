package actionpages;

import org.junit.Assert;
import utilities.BasePage;

import static pageobjects.HomepagePO.BTN_CREATE_CAMPAIGN;
import static pageobjects.HomepagePO.HOMEPAGE_HEADER;
import static pageobjects.LoginPO.*;
import static utilities.CommonFunctions.*;

public class Homepage extends BasePage {

    public void clickCreateCampaign() throws Exception {
        handleIframe(BTN_CREATE_CAMPAIGN);
        click(BTN_CREATE_CAMPAIGN);
    }
    
    @Override
    public void waitPageToLoad() {
        super.waitPageToLoad(FORM_LOGIN);
    }
}
