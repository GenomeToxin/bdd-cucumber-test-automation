package actionpages;

import model.CampaignType;
import utilities.BasePage;

import static pageobjects.CampaignBuilderPO.*;
import static pageobjects.HomepagePO.*;
import static utilities.CommonFunctions.*;

public class CampaignBuilder extends BasePage {

    public void clickCreateCampaign() {
        click(BTN_CREATE_CAMPAIGN);
        waitPageToLoad();
    }

    public void selectCampaignType(CampaignType campaignType) {
        switch (campaignType) {
            case INFLUENCER:
                click(BTN_CREATE_INFLUENCER_CAMPAIGN);
                break;
            case CONTENT:
                click(BTN_CREATE_CONTENT_CAMPAIGN);
                break;
        }
    }

    @Override
    public void waitPageToLoad() {
        super.waitPageToLoad();
    }
}
