package stepdefinitions;

import actionpages.Login;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import utilities.ScenarioContext;

public class LoginStepDef {

    Login login;

    public LoginStepDef() {
        this.login = new Login();
    }

    @Given("User enters email {string}")
    public void enterEmail(String email) {
        login.enterEmail(email);
    }

    @Given("User enters password {string}")
    public void enterPassword(String password) {
        login.enterPassword(password);
    }

    @Given("User clicks login button")
    public void clickLogin() {
        login.clickLogin();
    }

    @Then("User shall login successfully")
    public void verifyLoginSuccessful() {
        login.verifyLoginSuccessful();
    }
}
