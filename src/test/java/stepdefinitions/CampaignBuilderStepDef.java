package stepdefinitions;

import actionpages.CampaignBuilder;
import actionpages.Homepage;
import actionpages.Login;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.CampaignType;
import model.Influencer;
import utilities.ScenarioContext;

public class CampaignBuilderStepDef {

    CampaignBuilder campaignBuilder;
    Homepage homepage;

    public CampaignBuilderStepDef() {
        this.campaignBuilder = new CampaignBuilder();
        this.homepage = new Homepage();
    }

    @Given("User click Create Campaign")
    public void clickCreateCampaign() throws Exception {
        homepage.clickCreateCampaign();
    }

    @Given("User creates an {campaignType} Campaign")
    public void selectCampaignType(String campaignType) {
        CampaignType campaign = null;
        switch (campaignType.toUpperCase()) {
            case "INFLUENCER":
                campaign = CampaignType.INFLUENCER;
                break;
            case "CONTENT":
                campaign = CampaignType.CONTENT;
                break;
        }
        campaignBuilder.selectCampaignType(campaign);
    }
}
