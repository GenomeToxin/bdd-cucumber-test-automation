package stepdefinitions;

import actionpages.BrandDetails;
import io.cucumber.java.en.Given;
import model.Influencer;
import utilities.ScenarioContext;

import java.io.File;

import static utilities.CommonFunctions.*;

public class InfluencerBrandStepDef {

    private BrandDetails brandDetails;
    private Influencer influencer;
    private String logoPath;
    private ScenarioContext scenarioContext;

    public InfluencerBrandStepDef() {
        this.brandDetails = new BrandDetails();
        this.influencer = new Influencer();
        scenarioContext = new ScenarioContext();
        this.logoPath = getAbsolutePath("src/test/resources/files/Selenium_Logo.png");
    }

    @Given("User adds Influencer Brand Details")
    public void addDetails() {
        influencer.setLogoPath(logoPath);
        influencer.setBrandName("Selenium Webdriver " + randomString(3));
        this.scenarioContext.setContext("influencer", influencer);

        brandDetails.uploadLogo(influencer.getLogoPath());
        brandDetails.enterBrandName(influencer.getBrandName());
        brandDetails.clickSaveBrandLogo();
    }
}
