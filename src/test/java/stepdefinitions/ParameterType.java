package stepdefinitions;

import actionpages.Login;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class ParameterType {

    Login login;

    public ParameterType() {
    }

    @io.cucumber.java.ParameterType("Influencer|Content")
    public static String campaignType(String campaignType) {
        return campaignType;
    }
}
