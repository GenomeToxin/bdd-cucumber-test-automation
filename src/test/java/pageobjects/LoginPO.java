package pageobjects;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class LoginPO {
    public static final By FORM_LOGIN = xpath("//*[@name='sign-in']");
    public static final By TXT_EMAIL_LOGIN = xpath("//*[@name='email']");
    public static final By TXT_PASSWORD_LOGIN = xpath("//*[@name='password']");
    public static final By BTN_LOGIN = xpath("//*[@data-cy-element='log-in-button']//span");
}
