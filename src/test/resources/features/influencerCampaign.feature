Feature: Create Influencer Campaign

  Scenario: Create new influencer campaign valid inputs
    Given User enters email "luisa+uat@tribegroup.co"
    And User enters password "tribe@123"
    And User clicks login button
    Then User shall login successfully
    Given User click Create Campaign
    And User creates an Influencer Campaign
    And User adds Influencer Brand Details

